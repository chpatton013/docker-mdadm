FROM debian:jessie
ENV DEBIAN_FRONTEND noninteractive

# Variables ####################################################################
ENV md_directory "/dev/md"
ENV mdadm_create_volume "/usr/local/bin/create_volume"
ENV mdadm_assemble_volume "/usr/local/bin/assemble_volume"
ENV mdadm_stop_volume "/usr/local/bin/stop_volume"
ENV mdadm_resize_volume "/usr/local/bin/resize_volume"
ENV mdadm_add_device "/usr/local/bin/add_device"
ENV mdadm_remove_device "/usr/local/bin/remove_device"
ENV mdadm_fail_device "/usr/local/bin/fail_device"
ENV mdadm_add_bitmap "/usr/local/bin/add_bitmap"
ENV mdadm_remove_bitmap "/usr/local/bin/remove_bitmap"

# Packages #####################################################################
RUN apt-get update --quiet --quiet \
 && apt-get install --quiet --quiet --assume-yes \
    inotify-tools \
    mdadm \
 && rm --recursive --force /var/lib/apt/lists/*

# MDADM ########################################################################
COPY mdadm_create_volume.sh "${mdadm_create_volume}"
COPY mdadm_assemble_volume.sh "${mdadm_assemble_volume}"
COPY mdadm_stop_volume.sh "${mdadm_stop_volume}"
COPY mdadm_resize_volume.sh "${mdadm_resize_volume}"
COPY mdadm_add_device.sh "${mdadm_add_device}"
COPY mdadm_remove_device.sh "${mdadm_remove_device}"
COPY mdadm_fail_device.sh "${mdadm_fail_device}"
COPY mdadm_add_bitmap.sh "${mdadm_add_bitmap}"
COPY mdadm_remove_bitmap.sh "${mdadm_remove_bitmap}"
RUN chmod +x "${mdadm_create_volume}"
RUN chmod +x "${mdadm_assemble_volume}"
RUN chmod +x "${mdadm_stop_volume}"
RUN chmod +x "${mdadm_resize_volume}"
RUN chmod +x "${mdadm_add_device}"
RUN chmod +x "${mdadm_remove_device}"
RUN chmod +x "${mdadm_fail_device}"
RUN chmod +x "${mdadm_add_bitmap}"
RUN chmod +x "${mdadm_remove_bitmap}"

RUN mkdir --parents "${md_directory}"
VOLUME "${md_directory}"

# This should be the $md_directory variable, but we can't use the
# shell-interpreted version of CMD for signal-handling reasons.
CMD ["inotifywait", "--monitor", "/dev/md"]
