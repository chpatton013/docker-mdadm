#!/usr/bin/env bash

set -euo pipefail

function usage() {
   local script="$(basename "${BASH_SOURCE[0]}")"
   echo "$script: <volume> <size> [MDADM OPTIONS]"
   echo
   echo "Arguments:"
   echo "  volume: Absolute path to raid volume"
   echo "  size: New size of raid array"
   echo
   echo "Options:"
   echo "  -h, --help: display this message and exit"
}

if [ "$#" -lt 1 ]; then
   usage >&2
   exit 1
fi

if echo $@ | grep --silent --extended-regexp '^(-h|--help)$'; then
   usage
   exit 0
fi

volume="$1"
size="$2"
shift 2

mdadm --grow "$volume" --raid-devices="$size" $@
