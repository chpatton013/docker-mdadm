# Docker MDADM Server

Use this container to manage software RAID volumes.

## Summary

This container has several utility scripts that forward arguments onto the
appropriate `mdadm` command.

| Script            | Delegates to                                           |
|-------------------|--------------------------------------------------------|
| `create_volume`   | `mdadm --create <device> --raid-devices=<num-devices>` |
| `assemble_volume` | `mdadm --assemble <device>`                            |
| `stop_volume`     | `mdadm --stop <device>`                                |
| `resize_volume`   | `mdadm --grow <device> --raid-devices=<num-devices>`   |
| `add_device`      | `mdadm --manage <volume> --add <device>`               |
| `fail_device`     | `mdadm --manage <volume> --fail <device>`              |
| `remove_device`   | `mdadm --manage <volume> --remove <device>`            |
| `add_bitmap`      | `mdadm --grow <volume> --bitmap=internal`              |
| `remove_bitmap`   | `mdadm --grow <volume> --bitmap=none`                  |

Each of these scripts respond to `-h` and `--help` flags.

In order to maintain an environment in which decypted volumes can persist, you
must run this container without a command. This will cause the default command
to run, which blocks indefinitely. After the container is running, you can issue
additional commands with `docker exec`.

You can introduce persistent devices to this container by mounting volumes from
the host machine and from other containers:

* From host machine: `--volume=<host-machine-path>:<container-mount-path>`
* From other containers: `--volumes-from=<container-name>`

You should also use volumes that store the mdadm configuration
(`/etc/mdadm/mdadm.conf`) and backup files created during grow operations
(`resize_volume`, `add_bitmap`, `remove_bitmap`).

This container will expose `/dev/md` as a docker volume, which is the directory
that mdadm will use to mount most logical volumes. This will allow you to easily
forward those mounted volumes onto other containers.

## Usage

`device` refers to the absolute path of a block device.

`volume` refers to the absolute path of the raid volume.

```
# Start the container and wait for additional commands:
docker build --tag=chpatton013/docker-mdadm .
docker run --privileged --rm --name=mdadm docker-mdadm

# Create a new raid array:
docker exec mdadm create_volume <volume> --level=<raid-level> --raid-devices=<num-devices> <list-of-block-devices>

# Start a raid array:
docker exec mdadm assemble_volume <volume>

# Stop the raid array:
docker exec mdadm stop_volume <volume>

# Resize the raid array:
docker exec mdadm resize_volume <volume> <num-devices> --backup-file=/mnt/mdadm-backups/resize_volume

# Add a device to the raid array:
docker exec mdadm add_device <volume> <device>

# Remove a device from the raid array:
docker exec mdadm remove_device <volume> <device>
docker exec mdadm fail_device <volume> <device>

# Add a bitmap to a raid array:
docker exec mdadm add_bitmap <volume> --backup-file=/mnt/mdadm-backups/add_bitmap

# Remove the bitmap from the raid array:
docker exec mdadm remove_bitmap <volume> --backup-file=/mnt/mdadm-backups/remove_bitmap
```
